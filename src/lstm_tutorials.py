import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


def generate_dictionary(input_text:str):
    chars = tuple(set(input_text))
    dict_size = len(chars)
    int2char = dict(enumerate(chars))
    char2int = {ch: np.zeros([dict_size], dtype=int) for ii, ch in int2char.items()}

    for i in range(dict_size):
        char2int[int2char[i]][i] = 1

    encoded = np.array([char2int[ch] for ch in input_text])
    print(encoded)


    check_str = ''
    for e in encoded:
        check_str = check_str + (int2char[np.argmax(e)])

    try:
        assert(input_text == check_str)
    except AssertionError:
        print("AssertionError in generate_dictionary():\n"
              "Dictionary not created.")
        return None, None

    return char2int, int2char


def one_hot_encode_2(text, char2int):
    out = []
    for i in range(len(text)):
        try:
            out.append(char2int[text[i]])
        except KeyError:
            print("KeyError in one_hot_encode():\n"
                  "Encoded text not created")
            return

    return np.asarray(out)

# Torch LSTM input: 3 Dimensions - Full sequence, batch size, sequence length???

def get_batches(arr:np.ndarray, n_seqs, n_steps):
    '''Create a generator that returns batches of size
       n_seqs x n_steps from arr.

       Arguments
       ---------
       arr: Ndarrays you want to make batches from
       n_seqs: Batch size, the number of sequences per batch
       n_steps: Number of sequence steps per batch
    '''

    batch_size = n_seqs * n_steps
    n_batches = len(arr) // batch_size

    # Keep only enough characters to make full batches
    arr = arr[:n_batches * batch_size]

    # Reshape into n_seqs rows
    arr = arr.reshape((n_seqs, -1))

    for n in range(0, arr.shape[1], n_steps):

        # The features
        x = arr[:, n:n + n_steps]

        # The targets, shifted by one
        y = np.zeros_like(x)

        try:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, n + n_steps]
        except IndexError:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, 0]
        yield x, y


def encode_text(text):
    chars = tuple(set(text))

    int2char = dict(enumerate(chars))

    char2int = {ch: ii for ii, ch in int2char.items()}

    return np.array([char2int[ch] for ch in text])


def one_hot_encode(arr, n_labels):
    # Initialize the the encoded array
    one_hot = np.zeros((np.multiply(*arr.shape), n_labels), dtype=np.float32)

    # Fill the appropriate elements with ones
    one_hot[np.arange(one_hot.shape[0]), arr.flatten()] = 1.

    # Finally reshape it to get back to the original array
    one_hot = one_hot.reshape((*arr.shape, n_labels))

    return one_hot


class CharacterLSTM(nn.Module):
    # Recommended steps:
    # - Create and store the necessary dictionaries
    # - Define an LSTM layer that takes as params: an input size (the number of characters), a hidden layer size
    #   n_hidden, a number of layers n_layers, a dropout probability drop_prob, and a batch_first boolean (True, since
    #   we are batching)
    # - Define a dropout layer with dropout_prob
    # - Define a fully-connected layer with params: input size n_hidden and output size (the number of characters)
    # - Finally, initialize the weights

    def __init__(self, tokens, n_steps=100, hidden_dim = 256, n_layers = 2, drop_prob = 0.5, lr=0.001):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.drop_prob = drop_prob
        self.lr = lr

        # Creating character dictionaries
        self.chars = tokens
        self.int2char = dict(enumerate(self.chars))
        self.char2int = {ch:ii for ii, ch in self.int2char.items()}

        # Define the actual LSTM
        self.lstm = nn.LSTM(input_size=len(self.chars),
                            hidden_size=self.hidden_dim,
                            num_layers=self.n_layers,
                            dropout=self.drop_prob,
                            batch_first=True)

        # Define a dropout layer
        self.dropout_layer = nn.Dropout(self.drop_prob)

        # Define the fully-connected layer
        # This layer converts the hidden state to the output(of size equal to dictionary)
        self.fc_layer = nn.Linear(hidden_dim, len(self.chars))

        # Initialize weights
        self.init_weights()

    def forward(self, x, hc):
        """
        Perform a forward pass through the cell.
        :param x: Data input
        :param hc: hidden state and cell state
        :return: Cell output and new cell state and hidden state
        """

        # Get x and new (h, c) from the lstm
        x, (h, c) = self.lstm(x, hc)

        # Perform dropout on x
        x = self.dropout_layer(x)

        # Stack LSTM outputs using view (is this torch-only?)
        x = x.view(x.size()[0]*x.size()[1], self.hidden_dim)

        # Put x through fully connected layer
        x = self.fc_layer(x)

        # Return x as the output, and (h, c) as new states
        return x, (h,c)

    def predict(self, char, h=None, cuda=False, top_k=None):
        if cuda:
            self.cuda()
        else:
            self.cpu()

        if h is None:
            h = self.init_hidden(1)

        x = np.array([[self.char2int[char]]])
        x = one_hot_encode(x, len(self.chars))

        input = torch.from_numpy(x)

        if cuda:
            input = input.cuda()

        h = tuple([each.data for each in h])
        out, h = self.forward(input, h)

        p = F.softmax(out, dim=1).data

        if cuda:
            p = p.cpu()

        if top_k is None:
            top_ch = np.arange(len(self.chars))
        else:
            p, top_ch = p.topk(top_k)
            top_ch = top_ch.numpy().squeeze()

        p = p.numpy().squeeze()

        char = np.random.choice(top_ch, p=p/p.sum())

        return self.int2char[char], h

    def init_weights(self):
        """Initialize weights for fully connected layer."""
        initrange = 0.1

        # Set bias tensor to all zeros
        self.fc_layer.bias.data.fill_(0)
        # Fully-connected weights to random uniform
        self.fc_layer.weight.data.uniform_(-1, 1)

    def init_hidden(self, n_seqs):
        """Initialize hidden state."""
        # Create two new tensors with sizes (shapes??) n_layers * n_seqs * hidden_dim,
        # initialized to zero, for hidden state and cell state of LSTM
        weight = next(self.parameters()).data
        # TODO: This part derails from the tutorial, make sure it works
        return (weight.new_zeros(self.n_layers, n_seqs, self.n_hidden),
                weight.new_zeros(self.n_layers, n_seqs, self.n_hidden))



if __name__ == '__main__':
    text = 'Hello world, I am hungry.'
    encoded_text = encode_text(text)
    batches = get_batches(encoded_text, n_seqs=3,n_steps=4)
    print(next(batches))
    print(next(batches))
    print(next(batches))
    # char2int, int2char = generate_dictionary(input_text=text)
    # encoded_text = one_hot_encode(text=text, char2int=char2int)
    # batches = get_batches(encoded_text, n_steps=7, n_seqs=1)
    # batch = next(batches)
    # print()
    # print(batch)