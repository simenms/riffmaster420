def readChartFile(file):
  f = open('./C418 - Wet Hands/notes.chart')
  metadata = ''
  notes = ''
  line=''

  while '[Song]' not in line:
    line = f.readline()
  while '}' not in line:
    line = f.readline()
    metadata = metadata + line

  while '[ExpertSingle]' not in line:
    line = f.readline()
  while '}' not in line:
    line = f.readline()
    notes = notes + line
  
  data = {}
  data['Metadata'] = parseMeta(metadata)
  data['Notes'] = parseNotes(notes)

  return data

def parseMeta(string):
  string = string.replace('\"','')
  split_string = string.split('\n')
  data = {}
  for s in split_string:
    if '=' in s:
        d = s.split('=')
        data[d[0].strip()] = d[1].strip()
  return(data)

def parseNotes(string):
  return_table = []
  newline_split = string.split('\n')
  for line in newline_split:
    if '=' in line:
        time,data = line.split('=')
        d = data.strip().split(' ')
        return_table.append([time.strip(),d[0],d[1],d[2]])
  return return_table